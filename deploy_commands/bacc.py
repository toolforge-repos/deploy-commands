import json
import requests

DEPLOY_HOST = 'deployment.eqiad.wmnet'


def get_mwdebug_host():
    r = requests.get('https://noc.wikimedia.org/conf/debug.json')
    return r.json()['backends'][0]


def cheesy_table(row) -> str:
    res = ""

    col_widths = []
    for col in row:
        col_widths.append(len(col) + 2)

    for width in col_widths:
        res += "+" + "-" * width
    res += "+\n"

    for col in row:
        res += "| " + col + " "
    res += "|\n"

    for width in col_widths:
        res += "+" + "-" * width
    res += "+\n"

    return res


def get_bacc(id):
    try:
        int(id)
    except TypeError:
        return {'error': 'It seems the change id is not a number!'}
    id = int(id)

    r = requests.get('https://gerrit.wikimedia.org/r/changes/' + str(id))
    data = json.loads(r.text.split('\n')[1])

    subject = data['subject']
    project = data['project']
    branch = data['branch']
    if project == 'operations/mediawiki-config':
        pass
    elif project == 'mediawiki/core':
        if branch == 'master':
            return {'error': 'Patches on master branch of mediawiki can not be deployed'}
    elif project.startswith('mediawiki/extensions/') or data['project'].startswith('mediawiki/skins/'):
        if branch == 'master':
            return {'error': 'Patches on master branch of mediawiki can not be deployed'}
    else:
        return {'error': 'This tool only supports mediawiki repos :('}

    first_commands = "scap backport {}".format(id)

    mwdebug_host = get_mwdebug_host()

    confirmation_example = """The following changes are scheduled for backport:
{}Backport the changes? (y/n):""".format(cheesy_table([str(id), subject]))

    continue_to_sync_example = """Changes synced to: {}.
Please do any necessary checks before continuing.
Continue with sync? (y/n):""".format(mwdebug_host)

    revert_commands = "scap backport --revert {}".format(id)

    return {
        'deploy': [
            ('On {}:'.format(DEPLOY_HOST), first_commands),
            ("You will be prompted for confirmation.  Answer 'y' to proceed:", confirmation_example),
            ("The change will be +2'd and scap backport will wait until the change is merged.  Then the change will be pulled down the deploy server and synced to testservers.  You will be prompted for confirmation to proceed.  Before answering 'y', test your changes on {}".format(mwdebug_host), continue_to_sync_example),
            ("Answer 'y' to the prompt after you have tested your changes on {} and your change will be deployed".format(mwdebug_host),
             """19:50:09 Started sync-apaches\n..."""),
        ],
        'revert': ('On {}:'.format(DEPLOY_HOST), revert_commands),
    }
